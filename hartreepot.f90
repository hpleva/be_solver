module hartreepot

  use types, only: dp
  use data, only: Nmesh,r,Dr,dx
  use trapezoidalrule, only: trapz7

  implicit none
  private
  public hartree

contains

  function hartree(rho) result(vh)
    ! Solves the equation V''(r) + 2/r*V'(r) = -4*pi*rho
    !
    ! Uses predictor corrector Adams method.
    !
    ! It rewrites it to the equivalent system of first order ODEs on a uniform
    ! grid:
    !   u1 = V
    !   u2 = V'
    !   u1p = u2 * Rp
    !   u2p = -(4*pi*rho + 2*u2/r) * Rp
    ! and integrates outward using Adams method. The initial conditions are:
    !   V (R(1)) = u1(1) = 4*pi * \int r * rho(r) dr
    !   V'(R(1)) = u2(1) = 0
    real(dp), intent(in) :: rho(Nmesh)
    real(dp) :: vh(Nmesh)
    integer, parameter :: maxIt=2
    integer :: N,i,j
    real(dp), dimension(Nmesh) :: u1,u2,u1p,u2p
    N=size(r,1)
    ! Boundary conditions
    u1(1)=trapz7(rho/r,lbound(rho,1),ubound(rho,1))
    u2(1)=0.0_dp
    ! Calculate first four points with RK4, then
    ! switch to more accurate Adam's method
    call hartreeRK4(u1(1:4),u2(1:4),rho)
    u1p(1:4)=Dr(1:4)*u2(1:4)
    u2p(1:4)=-Dr(1:4)*rho(1:4)/r(1:4)**2-2.0_dp*Dr(1:4)*u2(1:4)/r(1:4)
    do i=4,N-1
       u1(i+1)=u1(i)+AdamsExtraOutward(u1p,i)
       u2(i+1)=u2(i)+AdamsExtraOutward(u2p,i)
       do j=1,maxIt
          u1p(i+1)=Dr(i+1)*u2(i+1)
          u2p(i+1)=-Dr(i+1)*Rho(i+1)/r(i+1)**2-2.0*Dr(i+1)/r(i+1)*u2(i+1)
          u1(i+1)=u1(i)+AdamsInterOutward(u1p,i+1)
          u2(i+1)=u2(i)+AdamsInterOutward(u2p,i+1)
       end do
    end do
    vh=u1
  end function hartree

  subroutine hartreeRK4(u1,u2,rho)
    real(dp), intent(inout) :: u1(:),u2(:)
    real(dp), intent(in) :: rho(:)
    integer :: N,i
    real(dp) :: a1,a2,b1,b2,c1,c2,d1,d2
    N=size(u1,1)
    do i=1,N-1
       a1=Dr(i)*u2(i)
       a2=-Dr(i)*rho(i)/r(i)**2-2.0_dp*Dr(i)/r(i)*u2(i)
       b1=(Dr(i)+Dr(i+1))/2.0_dp*(u2(i)+a2/2.0_dp)
       b2=(-Dr(i)*Rho(i)/r(i)**2-Dr(i+1)*Rho(i+1)/r(i+1)**2)/2.0_dp+&
            (-2.0_dp*Dr(i)/r(i)-2.0_dp*Dr(i+1)/r(i+1))/2.0_dp*(u2(i)+a2/2.0_dp)
       c1=(Dr(i)+Dr(i+1))/2.0_dp*(u2(i)+b2/2.0_dp)
       c2=(-Dr(i)*Rho(i)/r(i)**2-Dr(i+1)*Rho(i+1)/r(i+1)**2)/2.0_dp+&
            (-2.0_dp*Dr(i)/r(i)-2.0_dp*Dr(i+1)/r(i+1))/2.0_dp*(u2(i)+b2/2.0_dp)
       d1=Dr(i+1)*(u2(i)+c2)
       d2=-Dr(i+1)*Rho(i+1)/r(i+1)**2-2.0_dp*Dr(i+1)/r(i+1)*(u2(i)+c2)
       u1(i+1)=u1(i)+1.0_dp/6.0_dp*(a1+2*b1+2*c1+d1)
       u2(i+1)=u2(i)+1.0_dp/6.0_dp*(a2+2*b2+2*c2+d2)
    end do
  end subroutine hartreeRK4

  function AdamsExtraOutward(y,i) result(r)
    real(dp), intent(in) :: y(:)
    integer, intent(in) :: i
    real(dp) :: r
    r=(55.0_dp*y(i)-59.0_dp*y(i-1)+37.0_dp*y(i-2)-9.0_dp*y(i-3))/24.0_dp
  end function AdamsExtraOutward

  function AdamsInterOutward(y,i) result(r)
    real(dp), intent(in) :: y(:)
    integer, intent(in) :: i
    real(dp) :: r
    r=(9.0_dp*y(i)+19.0_dp*y(i-1)-5.0_dp*y(i-2)+y(i-3))/24.0_dp
  end function AdamsInterOutward

end module hartreepot
