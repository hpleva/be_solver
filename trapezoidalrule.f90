module trapezoidalrule

  use types, only: dp
  use data, only: Dr,dx

  implicit none
  private
  public trapz7

contains

  function trapz7(array,indMin,indMax) result(s)
    real(dp), intent(in) :: array(:)
    integer, intent(in) :: indMin,indMax
    real(dp) :: g(indMax-indMin+1)
    real(dp) :: s
    integer :: N
    g=array(indMin:indMax)*Dr(indMin:indMax)
    N=size(g,1)
    s = (  36799 * (g(1) + g(N  ))  &
         + 176648 * (g(2) + g(N-1)) &
         +  54851 * (g(3) + g(N-2)) &
         + 177984 * (g(4) + g(N-3)) &
         +  89437 * (g(5) + g(N-4)) &
         + 130936 * (g(6) + g(N-5)) &
         + 119585 * (g(7) + g(N-6)) &
         ) / 120960
    s = s + sum(g(8:N-7))
  end function trapz7

end module trapezoidalrule
