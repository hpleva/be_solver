module xc

  use types, only: dp
  use constants, only: pi

  implicit none
  private
  public LDAvx,LDAvc,LDAex,LDAec

contains

  function rs2n(rs) result(n)
    real(dp), intent(in) :: rs(:)
    real(dp) :: n(size(rs))
    n=3.0_dp/(4.0_dp*pi*rs**3)
  end function rs2n

  function n2rs(n) result(rs)
    real(dp), intent(in) :: n(:)
    real(dp) :: rs(size(n))
    real(dp), parameter :: thrd=1.0_dp/3.0_dp
    rs=(3.0_dp/(4.0_dp*pi*n))**thrd
  end function n2rs

  function LDAvX(rho) result(vx)
    ! Calculates the LDA exchange potential (PER ELECTRON)
    use data, only: r
    real(dp), intent(in) :: rho(:)
    real(dp) :: vx(size(r)),n(size(r)),ex(size(r))
    real(dp), parameter :: ax=-0.738558766382022405884230032680836_dp
    real(dp), parameter :: thrd=1.0_dp/3.0_dp
    real(dp), parameter :: thrd4=4.0_dp/3.0_dp
    n=rho/4.0_dp/pi/r**2
    ex=ax*n**thrd
    vx=thrd4*ex
  end function LDAvX

  function LDAvC(rho) result(vc)
    ! Calculates the LDA correlation energy density (PER ELECTRON)
    use data, only: r
    real(dp), intent(in) :: rho(:)
    real(dp) :: vc(size(r)),n(size(r)),rs(size(r))
    n=rho/4.0_dp/pi/r**2
    rs=n2rs(n)
    vc=vcVWN(rs)
  end function LDAvC

  function LDAeX(rho) result(ex)
    ! Calculates the LDA exchange potential (PER ELECTRON)
    use data, only: r
    real(dp), intent(in) :: rho(:)
    real(dp) :: ex(size(r)),n(size(r))
    real(dp), parameter :: ax=-0.738558766382022405884230032680836_dp
    real(dp), parameter :: thrd=1.0_dp/3.0_dp
    n=rho/4.0_dp/pi/r**2
    ex=ax*n**thrd
  end function LDAeX

  function LDAeC(rho) result(ec)
    use data, only: r
    ! Calculates the LDA correlation energy density (PER ELECTRON)
    real(dp), intent(in) :: rho(:)
    real(dp) :: ec(size(r)),n(size(r)),rs(size(r))
    n=rho/4.0_dp/pi/r**2
    rs=n2rs(n)
    ec=ecVWN(rs)
  end function LDAeC

  function ecVWN(rs) result(ec)
    real(dp), intent(in) :: rs(:)
    real(dp) :: ec(size(rs)),y(size(rs)),yy(size(rs))    
    real(dp), parameter :: y0=-0.10498_dp
    real(dp), parameter :: b=3.72744_dp
    real(dp), parameter :: c=12.9352_dp
    real(dp), parameter :: A=0.0621814_dp
    real(dp) :: Q,yy0,term1(size(rs)),term2(size(rs)),term3a
    real(dp) :: term3b(size(rs)),term3c(size(rs))
    y=sqrt(rs)
    Q=sqrt(4.0_dp*c-b**2)
    yy=y**2+b*y+c
    yy0=y0**2+b*y0+c
    term1=log(y**2/yy)
    term2=2.0_dp*b/Q*atan(Q/(2.0_dp*y+b))
    term3a=b*y0/yy0
    term3b=log((y-y0)**2/yy)
    term3c=2.0_dp*(b+2.0_dp*y0)/Q*atan(Q/(2.0_dp*y+b))
    ec=A/2.0_dp*(term1+term2-term3a*(term3b+term3c))
  end function ecVWN

  function vcVWN(rs) result(vc)
    real(dp), intent(in) :: rs(:)
    real(dp) :: ec(size(rs)),vc(size(rs)),y(size(rs)),yy(size(rs))    
    real(dp), parameter :: y0=-0.10498_dp
    real(dp), parameter :: b=3.72744_dp
    real(dp), parameter :: c=12.9352_dp
    real(dp), parameter :: A=0.0621814_dp
    real(dp) :: Q,yy0,term1(size(rs)),term2(size(rs)),term3a
    real(dp) :: term3b(size(rs)),term3c(size(rs))
    y=sqrt(rs)
    Q=sqrt(4.0_dp*c-b**2)
    yy=y**2+b*y+c
    yy0=y0**2+b*y0+c
    term1=log(y**2/yy)
    term2=2.0_dp*b/Q*atan(Q/(2.0_dp*y+b))
    term3a=b*y0/yy0
    term3b=log((y-y0)**2/yy)
    term3c=2.0_dp*(b+2.0_dp*y0)/Q*atan(Q/(2.0_dp*y+b))
    ec=A/2.0_dp*(term1+term2-term3a*(term3b+term3c))
    vc=ec-A/6.0_dp*(c*(y-y0)-b*y0*y)/((y-y0)*yy)
  end function vcVWN

end module xc

!subroutine gcor2(A,A1,B1,B2,B3,B4,rtrs,GG,GGRS)
!real(dp), intent(in) :: A,A1,B1,B2,B3,B4,rtrs
!real(dp), intent(out) :: Q0,Q1,Q2,Q3,GG,GGRS
!    Q0 = -2.0*A*(1.0+A1*rtrs*rtrs)
!    Q1 = 2.0*A*rtrs*(B1+rtrs*(B2+rtrs*(B3+B4*rtrs)))
!    Q2 = log(1.0+1.0/Q1)
!    GG = Q0*Q2
!    Q3 = A*(B1/rtrs+2.0*B2+rtrs*(3.0*B3+4.0*B4*rtrs))
!    GGRS = -2.0*A*A1*Q2-Q0*Q3/(Q1*(1.0+Q1))
!end subroutine
