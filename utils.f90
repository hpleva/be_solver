module utils

  use types, only: dp

  implicit none
  private
  public savetxt

contains

  subroutine savetxt(filename, d)
    ! Saves a 2D array into a textfile.
    !
    ! Arguments
    ! ---------
    !
    character(len=*), intent(in) :: filename  ! File to save the array to
    real(dp), intent(in) :: d(:, :)           ! The 2D array to save
    !
    ! Example
    ! -------
    !
    ! real(dp) :: data(3, 2)
    ! call savetxt("log.txt", data)

    integer :: s, i
    open(newunit=s, file=trim(adjustl(filename)), status="replace")
    do i = 1, size(d, 1)
       write(s, *) d(i, :)
    end do
    close(s)
  end subroutine savetxt

end module utils
