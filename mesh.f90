module mesh

  use types, only: dp
  use data, only: Nmesh

  implicit none

  private
  public logmesh

contains

  subroutine logmesh(r,Dr,DDr,DDDr,rmin,rmax,a)
    real(dp), intent(in) :: rmin,rmax,a
    real(dp), intent(inout), allocatable :: r(:), Dr(:), DDr(:), DDDr(:)
    real(dp) :: alpha, beta
    integer :: N,i

    N=Nmesh-1
    allocate(r(N+1),Dr(N+1),DDr(N+1),DDDr(N+1))
    beta = log(a) / (N-1)
    alpha = (rmax-rmin)/(exp(beta*N)-1)
    do i=1,N+1
       r(i)    = alpha * (exp(beta*(i-1)) - 1) + rmin
       Dr(i)   = alpha * beta * exp(beta*(i-1))
       DDr(i)  = alpha * beta**2 * exp(beta*(i-1))
       DDDr(i) = alpha * beta**3 * exp(beta*(i-1))
    end do

  end subroutine logmesh

end module
