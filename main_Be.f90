program main_Be
  use types, only: dp
  use constants, only: pi
  use data
  use init, only: initialize
  use derivatives, only: genDcoeffs,xDiff1,xDiff2,xDiff3
  use derivatives, only: rDiff1,rDiff2,rDiff3
  use mesh, only: logmesh
  use trapezoidalrule, only: trapz7
  use xc, only: LDAvX,LDAvC,LDAeX,LDAeC
  use hartreepot, only: hartree
  use solverhovp, only: ofdft_step
  use utils, only: savetxt
  use energies, only: get_energies
  implicit none
  integer :: i,tmp,iterMain
  real(dp), allocatable :: f(:),dfdx(:),dfdr(:),d2fdr2(:),d3fdr3(:)
  real(dp) :: x0,h
  real(dp), allocatable :: vH(:),Rho(:),vXC(:)
  real(dp), allocatable :: RhoNew(:,:),gNew(:,:),vPNew(:,:),vKS(:),vExt(:)
  real(dp), allocatable :: RhoOld(:)
  real(dp) :: muNew,L2Norm,e1s
  real(dp), allocatable :: outArray(:,:)
  character(len=100) :: filename
  real(dp) :: Etot,EXC,ECoul,ENuc,EKin
  real(dp) :: ConvTarget,mix

  ! A program to solve the electronic structure of Be orbital-free
  ! with the accuracy of KS-DFT.

  !##############################################################################
  ! A block containing values for various parameters.
  !
  ! Numerical parameters
  itersMainMax=500 ! Max number of iterations in the main loop
  mix=0.1_dp
  ! Atomic information
  Z=4            ! Atomic number
  lambdas=[2,2]
  ls=[0,0]
  ! Logarithmic mesh:  
  aMesh=2.7e6_dp     ! a = r(last) / r(first): controls the change in point density  
  rmin=1.0e-7_dp ! Location of the first point
  rmax=50.0_dp   ! Location of the last point
  Nmesh=2001     ! Number of points
  !
  ! Accuracy of the finite difference approximations:
  ! stencil = number of points in the finite differences
  !         => stencil1=7 means 6th order accuracy in f'
  !         => stencil3=7 means 4th order accuracy in f''' etc.
  stencil1=7
  stencil2=7
  stencil3=7
  !
  ! Parameters of the beta- and gamma-grids
  Dbeta=0.4_dp ! Stepsize on the beta-grid
  Dgamma=0.1_dp
  NbgGrid=2      ! Number of points on the beta-grid
  ! Density and Pauli potential Adams method:
  AdamsInwardOrder=4
  AdamsOutwardOrder=4
  !##############################################################################
  !
  ! Initialize
  call initialize(vKS,vExt,RhoOld,RhoNew,gNew,vPNew,muNew)
  !
  ! Initial guess from the H-like orbitals
  RhoOld = (8.0_dp*exp(-2.0_dp*R*Z)*R**2.0_dp*Z**3.0_dp+&                                       
            exp(-R*Z)*R**2.0_dp*Z**3.0_dp*(1.0_dp-(R*Z)/2.0_dp)**2.0_dp)

  vH=hartree(RhoOld)
  vXC=LDAvX(RhoOld)+LDAvC(RhoOld)
  vKS=vExt+vH+vXC

  !
  ConvTarget=1.0e-10
  ! The main iteration loop can now begin
  do iterMain=1,itersMainMax
     print *,''
     print *,'Iteration number ',iterMain
     print *,''
     call ofdft_step(vKS,rhoNew,gNew,vPNew,muNew,e1s)
     ! Mixing
     RhoNew(:,1)=(1.0_dp-mix)*RhoOld+mix*RhoNew(:,1)
     ! New KS-potential
     vH=hartree(RhoNew(:,1))
     vXC=LDAvX(RhoNew(:,1))+LDAvC(RhoNew(:,1))
     vKS=vExt+vH+vXC
     ! Calculate the error in density (the L2 norm 
     ! of the difference between new and old).
     L2Norm=trapz7((RhoNew(:,1)-RhoOld)**2,1,Nmesh)
     write(*,'(A18,E12.6)') 'Error in density: ',L2Norm
     print *,''
     RhoOld=RhoNew(:,1)
     ! Check convergence
     if (L2Norm<ConvTarget) then
        write(*,'(A48)') '################################################'
        write(*,*) ''
        write(*,'(A33,I3,A12)') 'Convergence has been achieved in ',iterMain,' iterations.'
        write(*,*) ''
        call get_energies(Etot,EXC,ECoul,ENuc,EKin,RhoNew(:,1),gNew(:,1),vH,vExt,vPNew(:,1),muNew)
        write(*,'(A29,F11.6)') 'Total energy                =',Etot
        write(*,'(A29,F11.6)') 'Kinetic energy              =',EKin
        write(*,'(A29,F11.6)') 'Electron-electron energy    =',ECoul
        write(*,'(A29,F11.6)') 'Electron-nucleus energy     =',ENuc
        write(*,'(A29,F11.6)') 'Exchange-correlation energy =',EXC
        write(*,'(A40)') '----------------------------------------'
        write(*,'(A3,F10.6)') '1s:',e1s
        write(*,'(A3,F10.6)') '2s:',muNew
        write(*,*) ''
        exit
     end if
     ! Print the the energy components
     write(*,'(A48)') '################################################'
     write(*,*) ''
     !write(*,'(A33,I3,A12)') 'Convergence has been achieved in ',iterMain,' iterations.'
     !write(*,*) ''
     call get_energies(Etot,EXC,ECoul,ENuc,EKin,RhoNew(:,1),gNew(:,1),vH,vExt,vPNew(:,1),muNew)
     write(*,'(A29,F11.6)') 'Total energy                =',Etot
     write(*,'(A29,F11.6)') 'Kinetic energy              =',EKin
     write(*,'(A29,F11.6)') 'Electron-electron energy    =',ECoul
     write(*,'(A29,F11.6)') 'Electron-nucleus energy     =',ENuc
     write(*,'(A29,F11.6)') 'Exchange-correlation energy =',EXC
     write(*,'(A40)') '----------------------------------------'
     write(*,'(A3,F10.6)') '1s:',e1s
     write(*,'(A3,F10.6)') '2s:',muNew
     write(*,*) ''
  end do
  !

  ! Save calculated quantities to disk.
  !write(filename,*) './results'
  !allocate(outArray(Nmesh,3))
  !outArray(:,1)=r
  !outArray(:,2)=vPNew(:,1)
  !outArray(:,3)=vPNew(:,2)
  !outArray(:,2)=RhoNew(:,1)
  !outArray(:,3)=gNew(:,1)
  !outArray(:,4)=vPNew(:,1)
  !outArray(:,2)=orbs(1,:)
  !outArray(:,3)=orbs(2,:)
  !call savetxt(filename,outArray)

end program main_Be
