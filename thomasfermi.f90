module thomasfermi

  use types, only: dp
  use constants, only: pi

  implicit none
  private
  public TFpot

contains

  function TFpot() result(vTF)
    use data, only: Z,R
    ! Generalized Thomas-Fermi atomic potential
    real(dp) :: x(size(R,1)), Z_eff(size(R,1)), vTF(size(R,1))
    real(dp) :: alpha, beta, gamma

    x = R * (128*Z/(9*pi**2)) ** (1.0_dp/3)
    ! Z_eff(x) = Z * phi(x), where phi(x) satisfies the Thomas-Fermi equation:
    !   phi'' = phi**(3/2) / sqrt(x)
    ! with boundary conditions:
    !   phi(0)  = 1
    !   phi(oo) = 0
    ! There is no analytic solution, but one can solve this approximately. We use:
    ! http://arxiv.org/abs/physics/0511017
    alpha = 0.7280642371_dp
    beta = -0.5430794693_dp
    gamma = 0.3612163121_dp
    Z_eff = Z * (1 + alpha*sqrt(x) + beta*x*exp(-gamma*sqrt(x)))**2 * &
         exp(-2*alpha*sqrt(x))
    ! This keeps all the eigenvalues of the radial problem negative:
    where (Z_eff < 1) Z_eff = 1
    vTF = -Z_eff / R
  end function TFpot

end module thomasfermi
