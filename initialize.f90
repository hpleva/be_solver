module init

  use types, only: dp
  use thomasfermi, only: TFpot
  use mesh, only: logmesh
  use derivatives, only: genDcoeffs
  use data

  implicit none
  private
  public initialize

contains

  subroutine initialize(vKS,vExt,RhoOld,RhoNew,gNew,vPNew,muNew)
    real(dp), intent(inout), allocatable :: vKS(:),vExt(:),RhoOld(:)
    real(dp), intent(inout), allocatable :: RhoNew(:,:),gNew(:,:),vPNew(:,:)
    real(dp), intent(inout) :: muNew
    integer :: i
    ! Generate logarithmic mesh
    call logmesh(r,Dr,DDr,DDDr,rmin,rmax,aMesh)
    ! Generate finite difference coefficients
    dn1=stencil1-1
    dn2=stencil2-1
    dn3=stencil3-1
    dK1=dn1/2
    dK2=dn2/2
    dK3=dn3/2
    dx=1.0_dp
    Dcoeffs1=genDcoeffs(1,stencil1,dx)
    Dcoeffs2=genDcoeffs(2,stencil2,dx)
    Dcoeffs3=genDcoeffs(3,stencil3,dx)
    ! Generate beta and gamma grids
    !allocate(betaGrid(4))
    betaGrid=[(Dbeta*i, i=0,NbgGrid-1)]
    gammaGrid=[(Dgamma*i, i=0,NbgGrid-1)]
    ! Create arrays needed in the Adams method
    if (AdamsOutwardOrder==4) then
       DOutward=24.0_dp
       DPredOutward=24.0_dp
       DCorrOutward=24.0_dp
       !allocate(aOutward(4))
       !allocate(aPredOutward(4))
       !allocate(aCorrOutward(4))
       aPredOutward =(/-9.0_dp,37.0_dp,-59.0_dp,55.0_dp/)/DPredOutward
       aCorrOutward =(/1.0_dp,-5.0_dp,19.0_dp,9.0_dp/)/DCorrOutward
       aOutward     =(/1.0_dp,-5.0_dp,19.0_dp,9.0_dp/)/DOutward
       LambdaOutward=aOutward(ubound(aOutward,1))
       !allocate(fvPOutward(NbgGrid,4))
    else if (AdamsOutwardOrder==5) then
       DOutward=720.0_dp
       DPredOutward=720.0_dp
       DCorrOutward=720.0_dp
       !allocate(aOutward(5))
       !allocate(aPredOutward(5))
       !allocate(aCorrOutward(5))
       aPredOutward =(/251.0_dp,-1274.0_dp,2616.0_dp,-2774.0_dp,1901.0_dp/)/DPredOutward
       aCorrOutward =(/-19.0_dp,106.0_dp,-264.0_dp,646.0_dp,251.0_dp/)/DCorrOutward
       aOutward     =(/-19.0_dp,106.0_dp,-264.0_dp,646.0_dp,251.0_dp/)/DOutward
       LambdaOutward=aOutward(ubound(aOutward,1))
       !allocate(fvPOutward(NbgGrid,5))
    end if
    if (AdamsInwardOrder==4) then
       DInward=24.0_dp
       DPredInward=24.0_dp
       DCorrInward=24.0_dp
       !allocate(aInward(4))
       !allocate(aPredInward(4))
       !allocate(aCorrInward(4))
       aPredInward =(/-9.0_dp,37.0_dp,-59.0_dp,55.0_dp/)/DPredInward
       aCorrInward =(/1.0_dp,-5.0_dp,19.0_dp,9.0_dp/)/DCorrInward
       aInward     =(/1.0_dp,-5.0_dp,19.0_dp,9.0_dp/)/DInward
       LambdaInward=-aInward(ubound(aInward,1))
    else if (AdamsInwardOrder==5) then
       DInward=720.0_dp
       DPredInward=720.0_dp
       DCorrInward=720.0_dp
       !allocate(aInward(5))
       !allocate(aPredInward(5))
       !allocate(aCorrInward(5))
       aPredInward =(/251.0_dp,-1274.0_dp,2616.0_dp,-2774.0_dp,1901.0_dp/)/DPredInward
       aCorrInward =(/-19.0_dp,106.0_dp,-264.0_dp,646.0_dp,251.0_dp/)/DCorrInward
       aInward     =(/-19.0_dp,106.0_dp,-264.0_dp,646.0_dp,251.0_dp/)/DInward
       LambdaInward=-aInward(ubound(aInward,1))
    end if
    ! Allocate arrays
    allocate(vKS(Nmesh))
    allocate(vExt(Nmesh))
    allocate(RhoNew(Nmesh,NbgGrid))
    allocate(gNew(Nmesh,NbgGrid))
    allocate(vPNew(Nmesh,NbgGrid))
    allocate(Pin    (Nmesh,NbgGrid))
    allocate(Pdotin (Nmesh,NbgGrid))
    allocate(Pout   (Nmesh,NbgGrid))
    allocate(Pdotout(Nmesh,NbgGrid))
    allocate(Qin    (Nmesh,NbgGrid))
    allocate(Qdotin (Nmesh,NbgGrid))
    allocate(Qout   (Nmesh,NbgGrid))
    allocate(Qdotout(Nmesh,NbgGrid))
    allocate(vPin   (Nmesh,NbgGrid))
    allocate(vPout  (Nmesh,NbgGrid))
    allocate(fac    (Nmesh))
    allocate(facdot (Nmesh))
    allocate(Psol   (Nmesh,NbgGrid))
    allocate(Pdotsol(Nmesh,NbgGrid))
    allocate(vPsol  (Nmesh,NbgGrid))
    allocate(RhoOld(Nmesh))
    !
    Norbs=size(lambdas,1)
    allocate(orbs(Norbs,Nmesh))
    allocate(orbsNorms(Norbs))
    allocate(leigens(Norbs))
    leigens=ls*(ls+1)
    DeltaBeta=(/ 0,1,2,3 /)
    muNew=0.0_dp
    ! Initialize KS-potential:
    ! The initial guess for the KS-potential is
    ! Thomas-Fermi -potential + External Coulomb potential
    vExt=-Z/r
    vKS=TFpot()
  end subroutine initialize

end module init
