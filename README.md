# Orbital-free DFT solver for the Be atom.

Code is compiled using CMake by running the command `cmake . && make`.
System LAPACK is assumed to be available.
