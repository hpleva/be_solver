module solverhovp

  use types, only: dp
  use trapezoidalrule, only: trapz7

  implicit none
  private
  public ofdft_step

contains

  subroutine ofdft_step(vKS,rhoNew,gNew,vPNew,muNew,e1sOut)
    use data
    real(dp), intent(in) :: vKS(:)
    real(dp), intent(out) :: rhoNew(:,:),gNew(:,:),vPNew(:,:)
    real(dp), intent(out) :: muNew,e1sOut
    integer :: N,ctp,itere,itereMax,iterAdams,iterAdamsMax
    real(dp) :: gamma,e1s00,e1s0,e1s,vPmin00,vPmin0,vPmin,step
    real(dp) :: e1smin,e1smax,e1smid,vPminmin,vPminMax,vPminmid
    integer :: i,j
    real(dp) :: norm0,normfac0,Qdiff,Pint,mu,dmu,dmuTarget,e1sTarget
    real(dp) ::gammaMin,gammaMax,gammaMid,gammaTarget,e1sShift
    integer :: iterGamma,iterGammaMax
    real(dp) :: mumin,mumax,e1sMultiplier
    integer, parameter :: prnt=0

    !print *,'ofdft_step started'
    Pin=0.0_dp
    Pdotin=0.0_dp
    Pout=0.0_dp
    Pdotout=0.0_dp
    Qin=0.0_dp
    Qdotin=0.0_dp
    Qout=0.0_dp
    Qdotout=0.0_dp
    vPin=0.0_dp
    vPout=0.0_dp
    fac=0.0_dp
    facdot=0.0_dp
    orbs=0.0_dp
    !print *,'arrays initialized'
    
    mumin=muminDefault
    mumax=mumaxDefault
    dmu=0.0_dp
    dmuTarget=1.0e-7_dp
    mu=0.5_dp*(mumin+mumax)
    gammaMin=10_dp
    gammaMax=60_dp
    gamma=0.5_dp*(gammaMin+gammaMax)
    iterGammaMax=30
    gammaTarget=1.0e-6_dp
    ctp=ctp0
    !ctp=Nmesh-10

    ! First we must find the correct 1s orbital energy.
    ! Finding it is based on the fact that vP has the
    ! right asymptotic decay towards zero only when the
    ! correct 1s energy has been found. When 1s energy
    ! is chosen wrong the tail of the Pauli potential
    ! approaches the value mu-e1s. We can thus formulate
    ! finding the correct energy as a minimization problem.

    e1s00=0.0_dp
    vPmin00=0.0_dp
    e1s0=0.0_dp
    vPmin0=0.0_dp
    e1s=-4.2_dp
    step=0.01_dp
    itereMax=1000
    iterAdamsMax=20
    e1sTarget=1.0e-6_dp
    e1sShift=1.0e-5_dp
    e1sMultiplier=1.0_dp

    ! Starting from TH potential
    !e1s=-2.849007_dp
    !mu=-0.216434_dp
    !gamma=47.798473897919941_dp

    ! Starting from H-like orbitals
    !mu=-0.090_dp
    !mu=-0.039249_dp
    !e1s=-3.009855_dp
    !e1s=-3.01_dp
    !gamma=42.27944725494816_dp

    !print *,'mu,dmu,gamma,ctp = ',mu,dmu,gamma,ctp

    call AdamsOutwardCTP(Pout,Pdotout,Qout,Qdotout,orbs,vPout,mu,vKS,ctp,e1s,gamma)
    !call AdamsInward(Pin,Pdotin,Qin,Qdotin,vPin,mu,vKS,ctp)
    vPmin=vPout(ctp,1)
    if (prnt==1) then
       print *,'e1s,vPmin = ',e1s,vPmin
    end if

    !stop

    do itere=1,itereMax
       e1s00=e1s0
       vPmin00=vPmin0
       e1s0=e1s
       vPmin0=vPmin
       e1s=e1s+step
       call AdamsOutwardCTP(Pout,Pdotout,Qout,Qdotout,orbs,vPout,mu,vKS,ctp,e1s,gamma)
       vPmin=vPout(ctp,1)
       if (prnt==1) then
          print *,'e1s,vPmin,ctp = ',e1s,vPmin,ctp
       end if
       if (vPmin>vPmin0) then
          ! A bracket has been found. Exit the loop
          ! and refine using bisection method.
          e1smin=e1s00
          vPminmin=vPmin00
          e1smax=e1s
          vPminmax=vPmin
          exit
       end if
    end do
    e1smid=0.5_dp*(e1smin+e1smax)
    call AdamsOutwardCTP(Pout,Pdotout,Qout,Qdotout,orbs,vPout,mu,vKS,ctp,e1smid,gamma)
    vPminMid=vPout(ctp,1)
    do itere=1,itereMax
       if ((e1sMax-e1sMin)<e1sTarget) then
          if (prnt==1) then
             print *,'bisection: Final e1s = ',e1s
          end if
          exit
       end if
       if ((e1sMid-e1sMin)>=(e1sMax-e1sMid)) then
          e1s=0.5_dp*(e1sMin+e1sMid)
          call AdamsOutwardCTP(Pout,Pdotout,Qout,Qdotout,orbs,vPout,mu,vKS,ctp,e1s,gamma)
          vPmin=vPout(ctp,1)
          if (prnt==1) then
             print *,'bisection: e1s,vPmin,ctp = ',e1s,vPmin,ctp
          end if
          if (vPmin<vPminMid) then
             e1sMax=e1sMid
             vPminMax=vPminMid
             e1sMid=e1s
             vPminMid=vPmin
          else
             e1sMin=e1s
          end if
       else
          e1s=0.5_dp*(e1sMid+e1sMax)
          call AdamsOutwardCTP(Pout,Pdotout,Qout,Qdotout,orbs,vPout,mu,vKS,ctp,e1s,gamma)
          vPmin=vPout(ctp,1)
          if (prnt==1) then
             print *,'bisection: e1s,vPmin,ctp = ',e1s,vPmin,ctp
          end if
          if (vPmin<vPminMid) then
             e1sMin=e1sMid
             vPminMin=vPminMid
             e1sMid=e1s
             vPminMid=vPmin
          else
             e1sMax=e1s
          end if
       end if
    end do
    !
    ! 
    ! Now we are able to find the correct mu
    ! 
    do iterAdams=1,iterAdamsMax
       !print *,'Adams iteration number ',iterAdams
       call AdamsOutward(Pout,Pdotout,Qout,Qdotout,orbs,vPout,mu,vKS,ctp,e1s,gamma)
       call AdamsInward(Pin,Pdotin,Qin,Qdotin,vPin,mu,vKS,ctp)
       !print *,orbs(1,ctp-5:ctp+5)
       !print *,''
       !print *,orbs(1,1:20)
       ! Join the two solutions
       do j=1,NbgGrid
          fac(j)=Pout(ctp,j)/Pin(ctp,j)
          facdot(j)=Pdotout(ctp,j)/Pdotin(ctp,j)
          !print *,fac(j)
       end do
       Psol(:ctp,:)=Pout(:ctp,:)
       Pdotsol(:ctp,:)=Pdotout(:ctp,:)
       do j=1,NbgGrid
          Psol(ctp:,j)=Pin(ctp:,j)*fac(j)
          Pdotsol(ctp:,j)=Pdotin(ctp:,j)*facdot(j)
       end do
       vPsol(:ctp,:)=vPout(:ctp,:)
       vPsol(ctp:,:)=0.0
       ! Normalize
       norm0=trapz7(Psol(:,1)**2,1,Nmesh)
       normfac0=sqrt(real(Z,dp)/norm0)
       Psol=normfac0*Psol
       Pdotsol=normfac0*Pdotsol
       ! Construct orbitals
       orbs=Normfac0**2*orbs
       orbs(2,ctp:)=orbs(2,ctp)/Psol(ctp,1)**2*Psol(ctp:,1)**2
       !print *,orbs(1,1:20)
       do j=1,size(orbs,1)
          orbsNorms(j)=trapz7(orbs(j,:),1,Nmesh)
          !print *,'ofdft_step: orbsNorms = ',orbsNorms(j)
       end do
       !print *,'gamma,orbsNorms = ',gamma,orbsNorms
       ! Compute DD in rho from which correct mu is deduced.
       Qdiff=Qout(ctp,1)-Qin(ctp,1)*fac(1)
       Pint=trapz7(Psol(:,1),1,Nmesh)
       ! Sometimes dmu will be NaN so in such a case we have
       ! to make a small shift in 1s orbital energy.
       dmu=0.5_dp*Psol(ctp,1)*Qdiff/Pint
       if (prnt==1) then
          print *,'mu,dmu = ',mu,dmu
       end if
       !if (isnan(dmu)) then
       !if (dmu /= dmu) then
       !   e1s=e1s+e1sShift
       !   e1sMultiplier=(-1.0_dp)**e1sMultiplier*(e1sMultiplier+1.0_dp)
       !   e1sShift=e1sMultiplier*e1sShift
       !   print *,'New shifted e1s = ',e1s
       !   cycle
       !end if
       if (abs(dmu)<dmuTarget) then
          exit
       else if (dmu>0) then
          mumin=mu
          mu=0.5_dp*(mumin+mumax)
       else
          mumax=mu
          mu=0.5_dp*(mumin+mumax)
       end if
       !print *,'mu,dmu = ',mu,dmu
    end do
    !
    ! Lastly we need to find the correct gamma
    !
    do iterGamma=1,iterGammaMax
       gammaMid=0.5_dp*(gammaMin+gammaMax)
       call AdamsOutward(Pout,Pdotout,Qout,Qdotout,orbs,vPout,mu,vKS,ctp,e1s,gammaMid)
       call AdamsInward(Pin,Pdotin,Qin,Qdotin,vPin,mu,vKS,ctp)
       ! Join the two solutions
       do j=1,NbgGrid
          fac(j)=Pout(ctp,j)/Pin(ctp,j)
          facdot(j)=Pdotout(ctp,j)/Pdotin(ctp,j)
          !print *,fac(j)
       end do
       Psol(:ctp,:)=Pout(:ctp,:)
       Pdotsol(:ctp,:)=Pdotout(:ctp,:)
       do j=1,NbgGrid
          Psol(ctp:,j)=Pin(ctp:,j)*fac(j)
          Pdotsol(ctp:,j)=Pdotin(ctp:,j)*facdot(j)
       end do
       vPsol(:ctp,:)=vPout(:ctp,:)
       vPsol(ctp:,:)=0.0
       ! Normalize
       norm0=trapz7(Psol(:,1)**2,1,Nmesh)
       normfac0=sqrt(real(Z,dp)/norm0)
       Psol=normfac0*Psol
       Pdotsol=normfac0*Pdotsol    
       ! Construct orbitals
       orbs=Normfac0**2*orbs
       orbs(2,ctp:)=orbs(2,ctp)/Psol(ctp,1)**2*Psol(ctp:,1)**2
       do j=1,size(orbs,1)
          orbsNorms(j)=Trapz7(orbs(j,:),1,Nmesh)
          if (prnt==1) then
             print *,'ofdft_step: orbsNorms = ',orbsNorms(j)
          end if
       end do
       !print *,'gamma,orbsNorms = ',gammaMid,orbsNorms
       if (orbsNorms(1)<1.0_dp) then
          gammaMin=gammaMid
       else
          gammaMax=gammaMid
       end if
       if (abs(orbsNorms(1)-1.0_dp)<gammaTarget) then
          exit
       end if
       !if (orbsNorms(2)>1.0_dp) then
       !   gammaMin=gammaMid
       !else
       !   gammaMax=gammaMid
       !end if
       !if (abs(orbsNorms(2)-1.0_dp)<gammaTarget) then
       !   exit
       !end if
    end do
    !
    !print *,'ctp,r(ctp) = ',ctp,r(ctp)
    !print * ,orbs(1,:)
    !print *,'ofdft_step: e1s,mu = ',e1s,mu
    rhoNew=Psol**2
    gNew=2.0_dp*Psol*Pdotsol
    vPNew=vPsol
    muNew=mu
    e1sOut=e1s
  end subroutine ofdft_step

  subroutine AdamsOutwardCTP(P,Pdot,Q,Qdot,orbs,vP,mu,vKS,ctp,e1s,gamma)
    use data, only: Z,r,Dr,Nmesh,NbgGrid,AdamsOutwardOrder,Dbeta,Norbs,dx,LambdaOutward
    use data, only: betaGrid,aOutward,aPredOutward,aCorrOutward,ctp0
    real(dp), intent(out) :: P(:,:),Pdot(:,:),Q(:,:)
    real(dp), intent(out) :: Qdot(:,:),vP(:,:),orbs(Norbs,Nmesh)
    real(dp), intent(in) :: mu,vKS(:),e1s,gamma
    integer, intent(inout) :: ctp
    real(dp) :: ccc,ddd,eCoeff,P00,Q00
    real(dp), dimension(Nmesh,NbgGrid) :: CC,Pp,Qp
    real(dp) :: DvP(NbgGrid),fvPOutward(AdamsOutwardOrder,NbgGrid)
    integer :: i,j,PECEiterMax,sigma,endPoint,PECEiter,vPMaxIndex
    real(dp) :: Ptmp,Qtmp,Delta,M11,M12,M21,M22
    real(dp) :: test(size(vP(1,:),1))
    ctp=ctp0
    vPMaxIndex=0
    PECEiterMax=1
    sigma=1
    orbs=0.0_dp
    ccc=2.0_dp
    ddd=1.0_dp
    eCoeff=mu-e1s
    P00=ccc*r(1)*(1.0_dp-Z*r(1))
    Q00=ddd*(ccc-2.0_dp*ccc*Z*r(1))
    test=[0.0_dp,-1.0e-1_dp]
    vP(1,:)=gamma*eCoeff/(gamma+exp(betaGrid(:)*eCoeff))
    vP(1,:)=0.0_dp
    !vP(1,:)=gamma*eCoeff/(gamma+exp(betaGrid(:)*eCoeff))+test(:)
    P(1,:)=sqrt(gamma*exp(betaGrid(:)*e1s)+exp(betaGrid(:)*mu))/sqrt(gamma+1.0_dp)*P00
    Q(1,:)=sqrt(gamma*exp(betaGrid(:)*e1s)+exp(betaGrid(:)*mu))/sqrt(gamma+1.0_dp)*Q00
    do j=1,NbgGrid
       vP(1:AdamsOutwardOrder,j)=vP(1,j)
       call OutwardRK4(P(1:AdamsOutwardOrder,j),Q(1:AdamsOutwardOrder,j),mu,&
            vKS(1:AdamsOutwardOrder),vP(1:AdamsOutwardOrder,j))
       CC(1:AdamsOutwardOrder,j)=-2.0_dp*(mu-vKS(1:AdamsOutwardOrder)-vP(1:AdamsOutwardOrder,j))
       Pp(1:AdamsOutwardOrder,j)=Dr(1:AdamsOutwardOrder)*Q(1:AdamsOutwardOrder,j)
       Qp(1:AdamsOutwardOrder,j)=Dr(1:AdamsOutwardOrder)*CC(1:AdamsOutwardOrder,j)*P(1:AdamsOutwardOrder,j)
    end do
    do i=1,AdamsOutwardOrder
       call DiffBeta(Pdot(i,:),Qdot(i,:),orbs(:,i),P(i,:),Q(i,:),e1s,mu)
       !print *,Qdot(:,i)
    end do
    endPoint=ctp-1
    !endPoint=10
    do i=AdamsOutwardOrder,endPoint
       do j=1,NbgGrid
          fvPOutward(:,j)=Dr(i-AdamsOutwardOrder+1:i)*4*((mu-vP(i-AdamsOutwardOrder+1:i,j))*&
               P(i-AdamsOutwardOrder+1:i,j)*Q(i-AdamsOutwardOrder+1:i,j)-&
               Pdot(i-AdamsOutwardOrder+1:i,j)*Q(i-AdamsOutwardOrder+1:i,j)-&
               P(i-AdamsOutwardOrder+1:i,j)*Qdot(i-AdamsOutwardOrder+1:i,j))/&
               P(i-AdamsOutwardOrder+1:i,j)**2
          DvP(j)=MultistepOutward(fvPOutward(:,j),1,aPredOutward)
          vP(i+1,j)=vP(i,j)+DvP(j)
          CC(i+1,j)=-2.0_dp*(mu-vKS(i+1)-vP(i+1,j))
          Pp(i,j)=Dr(i)*Q(i,j)
          Qp(i,j)=Dr(i)*CC(i,j)*P(i,j)
          Ptmp=P(i,j)+sigma*dx*MultistepOutwardImp(Pp(:,j),i,aOutward)
          Qtmp=Q(i,j)+sigma*dx*MultistepOutwardImp(Qp(:,j),i,aOutward)
          Delta=1.0_dp-LambdaOutward**2*CC(i+1,j)*Dr(i+1)**2
          M11 = 1.0_dp/Delta
          M21 = LambdaOutward*CC(i+1,j)*Dr(i+1)/Delta
          M12 = LambdaOutward*Dr(i+1)/Delta
          M22 = 1.0_dp/Delta
          P(i+1,j)=M11*Ptmp+M12*Qtmp
          Q(i+1,j)=M21*Ptmp+M22*Qtmp
       end do
       call DiffBeta(Pdot(i+1,:),Qdot(i+1,:),orbs(:,i+1),P(i+1,:),Q(i+1,:),e1s,mu)
       do PECEiter=1,PECEiterMax
          do j=1,NbgGrid
             fvPOutward(:,j)=Dr(i-AdamsOutwardOrder+2:i+1)*4*((mu-vP(i-AdamsOutwardOrder+2:i+1,j))*&
                  P(i-AdamsOutwardOrder+2:i+1,j)*Q(i-AdamsOutwardOrder+2:i+1,j)-&
                  Pdot(i-AdamsOutwardOrder+2:i+1,j)*Q(i-AdamsOutwardOrder+2:i+1,j)-&
                  P(i-AdamsOutwardOrder+2:i+1,j)*Qdot(i-AdamsOutwardOrder+2:i+1,j))/&
                  P(i-AdamsOutwardOrder+2:i+1,j)**2
             DvP(j)=MultistepOutward(fvPOutward(:,j),1,aCorrOutward)
             vP(i+1,j)=vP(i,j)+DvP(j)
             CC(i+1,j)=-2.0_dp*(mu-vKS(i+1)-vP(i+1,j))
             Pp(i,j)=Dr(i)*Q(i,j)
             Qp(i,j)=Dr(i)*CC(i,j)*P(i,j)
             Ptmp=P(i,j)+sigma*dx*MultistepOutwardImp(Pp(:,j),i,aOutward)
             Qtmp=Q(i,j)+sigma*dx*MultistepOutwardImp(Qp(:,j),i,aOutward)
             Delta=1.0_dp-LambdaOutward**2*CC(i+1,j)*Dr(i+1)**2
             M11 = 1.0_dp/Delta
             M21 = LambdaOutward*CC(i+1,j)*Dr(i+1)/Delta
             M12 = LambdaOutward*Dr(i+1)/Delta
             M22 = 1.0_dp/Delta
             P(i+1,j)=M11*Ptmp+M12*Qtmp
             Q(i+1,j)=M21*Ptmp+M22*Qtmp
          end do
          call DiffBeta(Pdot(i+1,:),Qdot(i+1,:),orbs(:,i+1),P(i+1,:),Q(i+1,:),e1s,mu)

          !print *,'PECEiter, i+1, P(i+1,1) = ',PECEiter,i,P(i+1,1)

       end do
       !
       !print *,DvP(1)
       !
       if (i+1>100 .and. vPMaxIndex == 0) then
          if (vP(i+1,1)<vP(i,1)) then
             vPMaxIndex=i
          end if
       end if
       if (vPMaxIndex /= 0) then
          !print *,vP(i,1),vP(i+1,1)
          if (vP(i+1,1)>vP(i,1) .or. vP(i+1,1)<0.0_dp) then
             ctp=i
             !print *,'vPMaxIndex,vPMax,ctp,vPMin = ',vPMaxIndex,vP(vPMaxIndex,1),ctp,vP(ctp,1)
             exit
          end if
       end if
    end do
    !do i=ctp-20,ctp
    !    print *,P(1,i)
    !end do
  end subroutine AdamsOutwardCTP

  subroutine AdamsOutward(P,Pdot,Q,Qdot,orbs,vP,mu,vKS,ctp,e1s,gamma)
    use data, only: Z,r,Dr,Nmesh,NbgGrid,AdamsOutwardOrder,Dbeta,Norbs,dx,LambdaOutward
    use data, only: betaGrid,aOutward,aPredOutward,aCorrOutward
    real(dp), intent(out) :: P(:,:),Pdot(:,:),Q(:,:)
    real(dp), intent(out) :: Qdot(:,:),vP(:,:),orbs(Norbs,Nmesh)
    real(dp), intent(in) :: mu,vKS(:),e1s,gamma
    integer, intent(in) :: ctp
    real(dp) :: ccc,ddd,eCoeff,P00,Q00
    real(dp), dimension(Nmesh,NbgGrid) :: CC,Pp,Qp
    real(dp) :: DvP(NbgGrid),fvPOutward(AdamsOutwardOrder,NbgGrid)
    integer :: i,j,PECEiterMax,sigma,endPoint,PECEiter
    real(dp) :: Ptmp,Qtmp,Delta,M11,M12,M21,M22
    real(dp) :: test(size(vP(1,:),1))
    PECEiterMax=1
    sigma=1
    orbs=0.0_dp
    ccc=2.0_dp
    ddd=1.0_dp
    eCoeff=mu-e1s
    P00=ccc*r(1)*(1.0_dp-Z*r(1))
    Q00=ddd*(ccc-2.0_dp*ccc*Z*r(1))
    test=[0.0_dp,-1.0e-1_dp]
    !vP(1,:)=gamma*eCoeff/(gamma+exp(betaGrid(:)*eCoeff))
    vP(1,:)=0.0_dp
    !vP(1,:)=gamma*eCoeff/(gamma+exp(betaGrid(:)*eCoeff))+test(:)
    P(1,:)=sqrt(gamma*exp(betaGrid(:)*e1s)+exp(betaGrid(:)*mu))/sqrt(gamma+1.0_dp)*P00
    Q(1,:)=sqrt(gamma*exp(betaGrid(:)*e1s)+exp(betaGrid(:)*mu))/sqrt(gamma+1.0_dp)*Q00
    do j=1,NbgGrid
       vP(1:AdamsOutwardOrder,j)=vP(1,j)
       call OutwardRK4(P(1:AdamsOutwardOrder,j),Q(1:AdamsOutwardOrder,j),mu,&
            vKS(1:AdamsOutwardOrder),vP(1:AdamsOutwardOrder,j))
       CC(1:AdamsOutwardOrder,j)=-2.0_dp*(mu-vKS(1:AdamsOutwardOrder)-vP(1:AdamsOutwardOrder,j))
       Pp(1:AdamsOutwardOrder,j)=Dr(1:AdamsOutwardOrder)*Q(1:AdamsOutwardOrder,j)
       Qp(1:AdamsOutwardOrder,j)=Dr(1:AdamsOutwardOrder)*CC(1:AdamsOutwardOrder,j)*P(1:AdamsOutwardOrder,j)
    end do
    do i=1,AdamsOutwardOrder
       call DiffBeta(Pdot(i,:),Qdot(i,:),orbs(:,i),P(i,:),Q(i,:),e1s,mu)
       !print *,Qdot(:,i)
    end do
    endPoint=ctp-1
    do i=AdamsOutwardOrder,endPoint
       do j=1,NbgGrid
          fvPOutward(:,j)=Dr(i-AdamsOutwardOrder+1:i)*4*((mu-vP(i-AdamsOutwardOrder+1:i,j))*&
               P(i-AdamsOutwardOrder+1:i,j)*Q(i-AdamsOutwardOrder+1:i,j)-&
               Pdot(i-AdamsOutwardOrder+1:i,j)*Q(i-AdamsOutwardOrder+1:i,j)-&
               P(i-AdamsOutwardOrder+1:i,j)*Qdot(i-AdamsOutwardOrder+1:i,j))/&
               P(i-AdamsOutwardOrder+1:i,j)**2
          DvP(j)=MultistepOutward(fvPOutward(:,j),1,aPredOutward)
          vP(i+1,j)=vP(i,j)+DvP(j)
          CC(i+1,j)=-2.0_dp*(mu-vKS(i+1)-vP(i+1,j))
          Pp(i,j)=Dr(i)*Q(i,j)
          Qp(i,j)=Dr(i)*CC(i,j)*P(i,j)
          Ptmp=P(i,j)+sigma*dx*MultistepOutwardImp(Pp(:,j),i,aOutward)
          Qtmp=Q(i,j)+sigma*dx*MultistepOutwardImp(Qp(:,j),i,aOutward)
          Delta=1.0_dp-LambdaOutward**2*CC(i+1,j)*Dr(i+1)**2
          M11 = 1.0_dp/Delta
          M21 = LambdaOutward*CC(i+1,j)*Dr(i+1)/Delta
          M12 = LambdaOutward*Dr(i+1)/Delta
          M22 = 1.0_dp/Delta
          P(i+1,j)=M11*Ptmp+M12*Qtmp
          Q(i+1,j)=M21*Ptmp+M22*Qtmp
       end do
       call DiffBeta(Pdot(i+1,:),Qdot(i+1,:),orbs(:,i+1),P(i+1,:),Q(i+1,:),e1s,mu)
       do PECEiter=1,PECEiterMax
          do j=1,NbgGrid
             fvPOutward(:,j)=Dr(i-AdamsOutwardOrder+2:i+1)*4*((mu-vP(i-AdamsOutwardOrder+2:i+1,j))*&
                  P(i-AdamsOutwardOrder+2:i+1,j)*Q(i-AdamsOutwardOrder+2:i+1,j)-&
                  Pdot(i-AdamsOutwardOrder+2:i+1,j)*Q(i-AdamsOutwardOrder+2:i+1,j)-&
                  P(i-AdamsOutwardOrder+2:i+1,j)*Qdot(i-AdamsOutwardOrder+2:i+1,j))/&
                  P(i-AdamsOutwardOrder+2:i+1,j)**2
             DvP(j)=MultistepOutward(fvPOutward(:,j),1,aCorrOutward)
             vP(i+1,j)=vP(i,j)+DvP(j)
             CC(i+1,j)=-2.0_dp*(mu-vKS(i+1)-vP(i+1,j))
             Pp(i,j)=Dr(i)*Q(i,j)
             Qp(i,j)=Dr(i)*CC(i,j)*P(i,j)
             Ptmp=P(i,j)+sigma*dx*MultistepOutwardImp(Pp(:,j),i,aOutward)
             Qtmp=Q(i,j)+sigma*dx*MultistepOutwardImp(Qp(:,j),i,aOutward)
             Delta=1.0_dp-LambdaOutward**2*CC(i+1,j)*Dr(i+1)**2
             M11 = 1.0_dp/Delta
             M21 = LambdaOutward*CC(i+1,j)*Dr(i+1)/Delta
             M12 = LambdaOutward*Dr(i+1)/Delta
             M22 = 1.0_dp/Delta
             P(i+1,j)=M11*Ptmp+M12*Qtmp
             Q(i+1,j)=M21*Ptmp+M22*Qtmp
          end do
          call DiffBeta(Pdot(i+1,:),Qdot(i+1,:),orbs(:,i+1),P(i+1,:),Q(i+1,:),e1s,mu)
       end do
       !print *,orbs(:,i+1)
    end do
    !do i=ctp-20,ctp
    !    print *,P(1,i)
    !end do
  end subroutine AdamsOutward

  subroutine AdamsInward(P,Pdot,Q,Qdot,vP,mu,vKS,ctp)
    use data, only: r,Dr,Nmesh,NbgGrid,AdamsInwardOrder,Dbeta,Norbs,dx,LambdaInward
    use data, only: betaGrid,aInward,aPredInward,aCorrInward
    real(dp), intent(out) :: P(:,:),Pdot(:,:),Q(:,:)
    real(dp), intent(out) :: Qdot(:,:),vP(:,:)
    real(dp), intent(in) :: mu,vKS(:)
    integer, intent(in) :: ctp
    real(dp) :: ccc,ddd,eCoeff,P00,Q00
    real(dp) :: P0(NbgGrid),Q0(NbgGrid),vP0(NbgGrid)
    real(dp), dimension(Nmesh,NbgGrid) :: CC,Pp,Qp
    real(dp) :: DvP(NbgGrid),fvPInward(NbgGrid,AdamsInwardOrder)
    integer :: i,j,sigma,endPoint,iMax
    real(dp) :: Ptmp,Qtmp,Delta,M11,M12,M21,M22,Lam,epsilon
    real(dp) :: Normi
    integer :: prnt
    iMax=Nmesh-AdamsInwardOrder+1
    sigma=-1
    Lam=sqrt(-2.0_dp*mu)
    epsilon=0.0_dp
    ! Initial values
    do j=1,NbgGrid
       vP(iMax:Nmesh,j)   =epsilon
       P(iMax:Nmesh,j)    =exp(0.5_dp*betaGrid(j)*mu)*exp(-Lam*(r(iMax:Nmesh)-r(1)))
       Q(iMax:Nmesh,j)    =-Lam*P(iMax:Nmesh,j)
       Pp(iMax:Nmesh,j)   =Dr(iMax:Nmesh)*Q(iMax:Nmesh,j)
       Qp(iMax:Nmesh,j)   =Dr(iMax:Nmesh)*(-2.0_dp*(mu-vKS(iMax:Nmesh)-&
                           vP(iMax:Nmesh,j)))*P(iMax:Nmesh,j)
       Pdot(iMax:Nmesh,j) =0.5_dp*mu*P(iMax:Nmesh,j)
       Qdot(iMax:Nmesh,j) =-Lam*Pdot(iMax:Nmesh,j)
       CC(iMax:Nmesh,j)   =-2.0_dp*(mu-vKS(iMax:Nmesh)-vP(iMax:Nmesh,j))
    end do
    
    !print *,P(1,iMax:Nmesh)
    
    endPoint=ctp+1
    prnt=1

    do i=iMax,endPoint,-1
       !print *,i,r(i)
       do j=1,NbgGrid
          vP(i-1,j)=0.0_dp
          CC(i-1,j)=-2.0_dp*(mu-vKS(i-1)-vP(i-1,j))
          Pp(i,j)=Dr(i)*Q(i,j)
          Qp(i,j)=Dr(i)*CC(i,j)*P(i,j)
          Ptmp=P(i,j)+sigma*dx*MultistepInwardImp(Pp(:,j),i,aInward)
          Qtmp=Q(i,j)+sigma*dx*MultistepInwardImp(Qp(:,j),i,aInward)
          Delta=1.0_dp-LambdaInward**2*CC(i-1,j)*Dr(i-1)**2
          M11 = 1.0_dp/Delta
          M21 = LambdaInward*CC(i-1,j)*Dr(i-1)/Delta
          M12 = LambdaInward*Dr(i-1)/Delta
          M22 = 1.0_dp/Delta
          P(i-1,j)=M11*Ptmp+M12*Qtmp
          Q(i-1,j)=M21*Ptmp+M22*Qtmp
          Pdot(i-1,j)=0.5_dp*mu*P(i-1,j)
          Qdot(i-1,j)=-Lam*Pdot(i-1,j)
          !if (j==1 .and. prnt==1) then
          !   print *,M11,M12,M21,M22
          !   prnt=0
          !end if
       end do
    end do
    !do i=Nmesh,Nmesh-20,-1
    !    print *,Pdot(1,i),Qdot(1,i)
    !end do
    !do i=20,2,-1
    !    print *,P(1,i),Q(1,i)
    !end do 
    ! Normalize
    !Normi=trapz7(P(1,:)**2,1,Nmesh)
    !print *,'Normi=',Normi
    !P=P/sqrt(Normi)
    !do i=Nmesh,1,-1
    !   print *,r(i),P(1,i)**2
    !end do
  end subroutine AdamsInward

  subroutine OutwardRK4(P,Q,mu,vKS,vP)
    use data, only: AdamsOutwardOrder,Dr
    real(dp), intent(inout) :: P(AdamsOutwardOrder),Q(AdamsOutwardOrder)
    real(dp), intent(in) :: mu,vKS(AdamsOutwardOrder),vP(AdamsOutwardOrder)
    integer :: ending,i
    real(dp) :: aP1,aP2,aP3,aP4,aQ1,aQ2,aQ3,aQ4
    ending=AdamsOutwardOrder-1
    do i=1,ending
       aP1=Dr(i)*Q(i)
       aQ1=-2.0_dp*Dr(i)*(mu-(vKS(i)+vP(i)))*P(i)
       aP2=0.5_dp*(Dr(i)+Dr(i+1))*(Q(i)+0.5_dp*aQ1)
       aQ2=(-2.0_dp*Dr(i)*(mu-(vKS(i)+vP(i)))-2.0_dp*Dr(i+1)*(mu-(vKS(i+1)+vP(i+1))))/2.0_dp*(P(i)+aP1/2.0_dp)
       aP3=(Dr(i)+Dr(i+1))/2.0_dp*(Q(i)+aQ2/2.0_dp)
       aQ3=(-2.0_dp*Dr(i)*(mu-(vKS(i)+vP(i)))-2.0_dp*Dr(i+1)*(mu-(vKS(i+1)+vP(i+1))))/2.0_dp*(P(i)+aP2/2.0_dp)
       aP4=Dr(i+1)*(Q(i)+aQ3)
       aQ4=-2.0_dp*Dr(i+1)*(mu-(vKS(i+1)+vP(i+1)))*(P(i)+aP3)
       P(i+1)=P(i)+1.0_dp/6.0_dp*(aP1+2.0_dp*aP2+2.0_dp*aP3+aP4)
       Q(i+1)=Q(i)+1.0_dp/6.0_dp*(aQ1+2.0_dp*aQ2+2.0_dp*aQ3+aQ4)
    end do
  end subroutine OutwardRK4

  function MultistepOutward(y,i,a) result(r)
    use data, only: Nmesh,AdamsOutwardOrder
    integer, intent(in) :: i
    real(dp), intent(in) :: y(Nmesh),a(AdamsOutwardOrder)
    real(dp) :: r
    integer :: k
    r=0.0_dp
    do k=1,AdamsOutwardOrder
       r=r+a(k)*y(i+k-1)
    end do
  end function MultistepOutward

  function MultistepOutwardImp(y,i,a) result(r)
    use data, only: Nmesh,AdamsOutwardOrder
    integer, intent(in) :: i
    real(dp), intent(in) :: y(Nmesh),a(AdamsOutwardOrder)
    real(dp) :: r
    integer :: k,index
    r=0.0_dp
    index=AdamsOutwardOrder-2
    do k=1,AdamsOutwardOrder-1
       r=r+a(k)*y(i-index+k-1)
    end do
  end function MultistepOutwardImp

  function MultistepInward(y,i,a) result(r)
    use data, only: Nmesh,AdamsInwardOrder
    integer, intent(in) :: i
    real(dp), intent(in) :: y(Nmesh),a(AdamsInwardOrder)
    real(dp) :: r
    integer :: k
    r=0.0_dp
    do k=AdamsInwardOrder,1,-1
       r=r+a(k)*y(i+k-1)
    end do
  end function MultistepInward

  function MultistepInwardImp(y,i,a) result(r)
    use data, only: Nmesh,AdamsInwardOrder
    integer, intent(in) :: i
    real(dp), intent(in) :: y(Nmesh),a(AdamsInwardOrder)
    real(dp) :: r
    integer :: k,index
    r=0.0_dp
    index=AdamsInwardOrder-2
    do k=1,AdamsInwardOrder-1
       r=r+a(k)*y(i+index-k+1)
    end do
  end function MultistepInwardImp

  subroutine DiffBeta(Pdot,Qdot,orbs,P,Q,e1s,mu)
    use data, only: betaGrid,gammaGrid,NbgGrid,Norbs,leigens,lambdas
    real(dp), intent(in) :: P(:),Q(:),e1s,mu
    real(dp), intent(inout) :: Pdot(:),Qdot(:),orbs(:)
    real(dp) :: orbsPrime(Norbs)
    integer :: N,NRHS,LDA,LDB,INFO
    real(dp) :: A(Norbs,Norbs),Awork(Norbs,Norbs),B(Norbs,2) 
    integer :: IPIV(Norbs)
    real(dp) :: rho(Norbs),rhoPrime(Norbs),energies(Norbs)
    integer :: i
    !
    rho=P**2
    rhoPrime=2.0_dp*P*Q
    energies=[e1s,mu]
    !
    N=Norbs
    NRHS=2
    LDA=N
    LDB=N
    !
    do i=1,Norbs
       A(i,:)=lambdas(:)*exp(betaGrid(i)*energies(:)-gammaGrid(i)*leigens(:))
    end do
    Awork=A
    B(:,1)=rho
    B(:,2)=rhoPrime
    call dgesv( N, NRHS, Awork, LDA, IPIV, B, LDB, INFO )
    !
    orbs=B(:,1)
    orbsPrime=B(:,2)
    !
    do i=1,Norbs
       Pdot(i)=0.5_dp*sum(energies(:)*A(i,:)*orbs(:))/P(i)
       Qdot(i)=0.5_dp*(sum(energies(:)*A(i,:)*orbsPrime(:))-2.0_dp*Q(i)*Pdot(i))/P(i)
    end do
    !
    !print *,Qdot
  end subroutine DiffBeta

end module solverhovp
