module energies

use types, only: dp
use trapezoidalrule, only: trapz7
use data, only: Nmesh,r
use xc, only: LDAeX,LDAeC
use derivatives, only: Weizsacker

implicit none
private
public get_energies

contains

subroutine get_energies(Etot,EXC,ECoul,ENuc,EKin,Rho,g,vH,vExt,vP,mu)
  real(dp), intent(in) :: Rho(Nmesh),g(Nmesh),vH(Nmesh)
  real(dp), intent(in) :: vExt(Nmesh),vP(Nmesh),mu
  real(dp), intent(out) :: Etot,EXC,ECoul,ENuc,EKin
  real(dp) :: eX(Nmesh),eC(Nmesh),tW(Nmesh),dTdn(Nmesh),tau(Nmesh)
  !
  eX=LDAeX(Rho)
  eC=LDAeC(Rho)
  EXC=trapz7((eX+eC)*Rho,1,Nmesh)
  ECoul=0.5_dp*trapz7(vH*Rho,1,Nmesh)
  ENuc=trapz7(vExt*Rho,1,Nmesh)
  ! The kinetic energy is calculated using Eq. (37)
  ! from J. Chem. Phys. 135, 044106 (2011)
  !
  ! Weizsaecker kinetic energy term
  tW=Weizsacker(Rho)
  ! Functional derivative of the non-interacting kinetic energy
  dTdn=tW+vP
  ! The ensemble non-interacting radial kinetic energy density
  ! (defined by Eqs. (23-24) in J. Chem. Phys. 135, 044106 (2011))
  tau=Rho*dTdn-mu*Rho+g
  Ekin=trapz7(tau,1,Nmesh)
  ! Total energy of the system
  Etot=EXC+ECoul+ENuc+Ekin
end subroutine get_energies

end module energies
