module data

  use types, only: dp
  implicit none

  integer :: Nmesh,dK1,dK2,dK3,dn1,dn2,dn3,Z,NbgGrid
  integer :: Nbeta,Ngamma,ctp0=1700
  real(dp) :: rmin,rmax,aMesh
  real(dp) :: Dbeta,Dgamma
  real(dp), allocatable :: betaGrid(:),gammaGrid(:)
  real(dp) :: dx,muminDefault=-0.5_dp,mumaxDefault=-0.02_dp
  real(dp), allocatable :: Dcoeffs1(:,:),Dcoeffs2(:,:),Dcoeffs3(:,:)
  real(dp), allocatable :: r(:),Dr(:),DDr(:),DDDr(:)
  real(dp), allocatable :: Pin(:,:),Pout(:,:),Qin(:,:),Qout(:,:),Pdotin(:,:)
  real(dp), allocatable :: Pdotout(:,:),Qdotin(:,:),Qdotout(:,:),vPin(:,:),vPout(:,:)
  real(dp), allocatable :: Psol(:,:),Pdotsol(:,:),vPsol(:,:)
  real(dp), allocatable :: fac(:),facdot(:)
  real(dp) :: LambdaOutward,LambdaInward
  real(dp) :: DOutward,DPredOutward,DCorrOutward
  real(dp), allocatable :: aOutward(:),aPredOutward(:),aCorrOutward(:)
  real(dp) :: DInward,DPredInward,DCorrInward
  real(dp), allocatable :: aInward(:),aPredInward(:),aCorrInward(:)
  real(dp), allocatable :: orbs(:,:),orbsNorms(:)
  integer, allocatable :: lambdas(:),ls(:),leigens(:),DeltaBeta(:)
  integer :: Norbs,stencil1,stencil2,stencil3
  integer :: itersMainMax,AdamsOutwardOrder,AdamsInwardOrder

end module data
